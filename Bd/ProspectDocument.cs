//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Db
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProspectDocument
    {
        public long ProspectFileId { get; set; }
        public Nullable<long> ProspectId { get; set; }
        public string FileRoute { get; set; }
        public string FileName { get; set; }
    
        public virtual Prospect Prospect { get; set; }
    }
}
