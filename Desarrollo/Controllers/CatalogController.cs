﻿using PagedList;
using Db;
using Desarrollo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core.Objects;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using DocumentFormat.OpenXml.Drawing.ChartDrawing;

namespace Desarrollo.Controllers
{
    //[Authorize]
    public class CatalogController : Controller
    {
           public ActionResult Prospects()
        {
            return View();
        }
        public ActionResult GetProspect(SearchViewModel filter, ProspectViewModel prospectFilter)
        {
            var rowCount = new ObjectParameter("total", typeof(int));
            var db = new EvaluationEntities();
            var display = filter.iDisplayStart == 0 ? 1 : (filter.iDisplayStart / filter.iDisplayLength) + 1;
            var prospects = db.Prospects.Where(s => s.Name.Contains(prospectFilter.Name)
                                                 || prospectFilter.Name == null);
            var result = from c in prospects.OrderBy(x => x.ProspectId).ToPagedList(display, filter.iDisplayLength)
                         select
                         new[]
                         {c.Name + ' ' + c.FathersLastName + ' ' + c.MothersLastName 
                         ,c.ProspectId.ToString()
                         ,c.StatusId.ToString()    
                         ,c.Observatoins
                         ,c.ProspectDocuments.Count().ToString()
                         ,c.Status.Description.ToString()
                         }.ToArray();
            return
                Json(
                    new
                    {
                        sEcho = filter.sEcho,
                        iTotalRecords = prospects.Count(),
                        iTotalDisplayRecords = prospects.Count(),
                        aaData = result
                    },
                    JsonRequestBehavior.AllowGet);
        }
        public ActionResult EditProspect(long ProspectId)
        {
            var db = new EvaluationEntities();
            var prosp = db.Prospects.Find(ProspectId);
            var model = new EditProspectView()
            {
                Name = prosp.Name
                ,
                FathersLastName = prosp.FathersLastName
                ,
                MothersLastName = prosp.MothersLastName
                ,
                Street = prosp.Street
                ,
                Number = prosp.Number
                ,
                ZipCode = prosp.ZipCode
                ,
                Colony = prosp.Colony
                ,
                PhoneNumber = prosp.PhoneNumber
                ,
                RFC = prosp.RFC         
            };
            return View(model);
        }
        public ActionResult EditProspectStatus(long ProspectId)
        {
            var db = new EvaluationEntities();
            var prospect = db.Prospects.Find(ProspectId);
            var model = new EditProspectStatusView();
            model.Statuses = new SelectList(new EvaluationEntities().Status.Select(x => new { StatusId = x.StatusId, Description = x.Description }), "StatusId", "Description");
            prospect.StatusId = model.StatusId;
            prospect.Observatoins = model.Observatoins;

            return View(model);
        }

        public ActionResult AddProspect()
        {
            return View("EditProspect");
        }

        [HttpPost]
        public JsonResult SaveProspectStatus(EditProspectStatusView model)
        {

            var db = new EvaluationEntities();
            Prospect model1;

                model1 = db.Prospects.Find(model.ProspectId);
            model1.StatusId = model.StatusId;
            model1.Observatoins = model.Observatoins;

            db.SaveChanges();

            return Json(model.ProspectId);
        }

        [HttpPost]
        public JsonResult SaveProspect(EditProspectView model)
        {

            var db = new EvaluationEntities();
            Prospect model1;

            if (model.ProspectId == 0)
            {
                Prospect prosp = new Prospect()
                {
                    Name = model.Name
                ,
                    FathersLastName = model.FathersLastName
                ,
                    MothersLastName = model.MothersLastName
                ,
                    Street = model.Street
                ,
                    Number = model.Number
                ,
                    ZipCode = model.ZipCode
                ,
                    Colony = model.Colony
                ,
                    PhoneNumber = model.PhoneNumber
                ,
                    RFC = model.RFC
                    ,
                    StatusId = model.StatusId
                };
                db.Prospects.Add(prosp);

            }
            else
            {
                model1 = db.Prospects.Find(model.ProspectId);
                model1.Name = model.Name;
                model1.FathersLastName = model.FathersLastName;
                model1.MothersLastName = model.MothersLastName;
                model1.Street = model.Street;
                model1.Number = model.Number;
                model1.ZipCode = model.ZipCode;
                model1.Colony = model.Colony;
                model1.PhoneNumber = model.PhoneNumber;
                model1.RFC = model.RFC;
            }

            db.SaveChanges();

            return Json(model.ProspectId);
        }

        public ActionResult GetProsDoc(SearchViewModel filter, EditProsDocView view)
        {
            var db = new EvaluationEntities();
            var rowCount = new ObjectParameter("total", typeof(int));            
            var display = filter.iDisplayStart == 0 ? 1 : (filter.iDisplayStart / filter.iDisplayLength) + 1;
            var Prosfiles = db.ProspectDocuments.Where(s => s.FileName.Contains(view.FileName) && s.ProspectId == view.ProspectId
                                                 || view.FileName == null && s.ProspectId == view.ProspectId);        
            var result = from c in Prosfiles.OrderBy(x => x.ProspectId).ToList().ToPagedList(display, filter.iDisplayLength)
                         select
                         new[]
                         {
                             c.FileName
                             ,
                             c.ProspectId.ToString()
                         }.ToArray();

            return
                 Json(
                     new
                     {     
                         iTotalRecords = Prosfiles.Count(),
                         iTotalDisplayRecords = Prosfiles.Count(),
                         aaData = result
                     },
                     JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditProsDo(long prospectId)
        {          
            var model = new EditProsDocView();
            model.ProspectId = prospectId;

            return View(model);
        }

        [HttpPost]
        public ActionResult addFilestoProspect(EditProsDocView model)
        {
            var db = new EvaluationEntities();

            string fileName = Path.GetFileNameWithoutExtension(model.File.FileName);

            string fileExtension = Path.GetExtension(model.File.FileName);

            fileName = model.ProspectId.ToString() + model.Prospect.RFC + fileExtension;

            string UploadPath = ConfigurationManager.AppSettings["ProspectPaths"].ToString();

            model.FilePath = UploadPath + fileName;

            model.File.SaveAs(model.FilePath);

            var doc = new ProspectDocument { FileName = model.FileName, ProspectId = model.ProspectId, FileRoute = model.FilePath };
            db.ProspectDocuments.Add(doc);

            db.SaveChanges();

            return View();
        }

    }
}