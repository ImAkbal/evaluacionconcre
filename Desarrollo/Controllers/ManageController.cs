﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Diagnostics.Instrumentation.Extensions.Intercept;
using Microsoft.Owin.Security;
using Db;
using Desarrollo.Controllers;
using Desarrollo.Models;

namespace Desarrollo.Controllers
{
    [Authorize]
    public class ManageController : Controller
    {
    //    private ApplicationSignInManager _signInManager;
    //    private ApplicationUserManager _userManager;

        public ManageController()
        {
        }

    //    public ManageController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
    //    {
    //        UserManager = userManager;
    //        SignInManager = signInManager;
    //    }

    //    public ApplicationSignInManager SignInManager
    //    {
    //        get
    //        {
    //            return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
    //        }
    //        private set
    //        {
    //            _signInManager = value;
    //        }
    //    }

    //    public ApplicationUserManager UserManager
    //    {
    //        get
    //        {
    //            return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
    //        }
    //        private set
    //        {
    //            _userManager = value;
    //        }



    //    }
        
        //
        // GET: /Manage/Index
        //public async Task<ActionResult> Index(ManageMessageId? message)
        //{
        //  var userId = User.Identity.GetUserId();
        //    var model = new IndexViewModel
        //    {
        //    };
        //    return View(model);
        //}

        //
        // POST: /Manage/RemoveLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> RemoveLogin(string loginProvider, string providerKey)
        //{
        //    ManageMessageId? message;
        //    var result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
        //    if (result.Succeeded)
        //    {
        //        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //        if (user != null)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //        }
        //        message = ManageMessageId.RemoveLoginSuccess;
        //    }
        //    else
        //    {
        //        message = ManageMessageId.Error;
        //    }
        //    return RedirectToAction("ManageLogins", new { Message = message });
        //}

        //
        // GET: /Manage/AddPhoneNumber
        //public ActionResult AddPhoneNumber()
        //{
        //    return View();
        //}

        //
        // POST: /Manage/AddPhoneNumber
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> AddPhoneNumber(AddPhoneNumberViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    // Generar el token y enviarlo
        //    var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), model.Number);
        //    if (UserManager.SmsService != null)
        //    {
        //        var message = new IdentityMessage
        //        {
        //            Destination = model.Number,
        //            Body = "Su código de seguridad es: " + code
        //        };
        //        await UserManager.SmsService.SendAsync(message);
        //    }
        //    return RedirectToAction("VerifyPhoneNumber", new { PhoneNumber = model.Number });
        //}

        //
        // POST: /Manage/EnableTwoFactorAuthentication
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> EnableTwoFactorAuthentication()
        //{
        //    await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), true);
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //    }
        //    return RedirectToAction("Index", "Manage");
        //}

        //
        // POST: /Manage/DisableTwoFactorAuthentication
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> DisableTwoFactorAuthentication()
        //{
        //    await UserManager.SetTwoFactorEnabledAsync(User.Identity.GetUserId(), false);
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //    }
        //    return RedirectToAction("Index", "Manage");
        //}

        //
        // GET: /Manage/VerifyPhoneNumber
        //public async Task<ActionResult> VerifyPhoneNumber(string phoneNumber)
        //{
        //    var code = await UserManager.GenerateChangePhoneNumberTokenAsync(User.Identity.GetUserId(), phoneNumber);
        //    // Enviar un SMS a través del proveedor de SMS para verificar el número de teléfono
        //    return phoneNumber == null ? View("Error") : View(new VerifyPhoneNumberViewModel { PhoneNumber = phoneNumber });
        //}

        //
        // POST: /Manage/VerifyPhoneNumber
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        ////public async Task<ActionResult> VerifyPhoneNumber(VerifyPhoneNumberViewModel model)
        ////{
        ////    if (!ModelState.IsValid)
        ////    {
        ////        return View(model);
        ////    }
        ////    var result = await UserManager.ChangePhoneNumberAsync(User.Identity.GetUserId(), model.PhoneNumber, model.Code);
        ////    if (result.Succeeded)
        ////    {
        ////        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        ////        if (user != null)
        ////        {
        ////            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        ////        }
        ////        return RedirectToAction("Index", new { Message = ManageMessageId.AddPhoneSuccess });
        ////    }
        //    // Si llegamos a este punto, es que se ha producido un error, volvemos a mostrar el formulario
        //    ModelState.AddModelError("", "No se ha podido comprobar el teléfono");
        //    return View(model);
        //}

        //
        // POST: /Manage/RemovePhoneNumber
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> RemovePhoneNumber()
        //{
        //    var result = await UserManager.SetPhoneNumberAsync(User.Identity.GetUserId(), null);
        //    if (!result.Succeeded)
        //    {
        //        return RedirectToAction("Index", new { Message = ManageMessageId.Error });
        //    }
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //    }
        //    return RedirectToAction("Index", new { Message = ManageMessageId.RemovePhoneSuccess });
        //}

        //
        // GET: /Manage/ChangePassword
        //public ActionResult ChangePassword()
        //{
        //    return View();
        //}

        //
        // POST: /Manage/ChangePassword
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return View(model);
        //    }
        //    var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
        //    if (result.Succeeded)
        //    {
        //        var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //        if (user != null)
        //        {
        //            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        //        }
        //        return RedirectToAction("Index", new { Message = ManageMessageId.ChangePasswordSuccess });
        //    }
        //    AddErrors(result);
        //    return View(model);
        //}

        //
        // GET: /Manage/SetPassword
        //public ActionResult SetPassword()
        //{
        //    return View();
        //}

        //
        // POST: /Manage/SetPassword
        ////[HttpPost]
        ////[ValidateAntiForgeryToken]
        ////public async Task<ActionResult> SetPassword(SetPasswordViewModel model)
        ////{
        ////    if (ModelState.IsValid)
        ////    {
        ////        var result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
        ////        if (result.Succeeded)
        ////        {
        ////            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        ////            if (user != null)
        ////            {
        ////                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
        ////            }
        ////            return RedirectToAction("Index", new { Message = ManageMessageId.SetPasswordSuccess });
        ////        }
        ////        AddErrors(result);
        ////    }

        //    // Si llegamos a este punto, es que se ha producido un error, volvemos a mostrar el formulario
        //    return View(model);
        //}

        //
        // GET: /Manage/ManageLogins
        //public async Task<ActionResult> ManageLogins(ManageMessageId? message)
        //{
        //    ViewBag.StatusMessage =
        //        message == ManageMessageId.RemoveLoginSuccess ? "Se ha quitado el inicio de sesión externo."
        //            : message == ManageMessageId.Error ? "Se ha producido un error."
        //                : "";
        //    var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
        //    if (user == null)
        //    {
        //        return View("Error");
        //    }
        //    var userLogins = await UserManager.GetLoginsAsync(User.Identity.GetUserId());
        //    var otherLogins = AuthenticationManager.GetExternalAuthenticationTypes().Where(auth => userLogins.All(ul => auth.AuthenticationType != ul.LoginProvider)).ToList();
        //    ViewBag.ShowRemoveButton = user.PasswordHash != null || userLogins.Count > 1;
        //    return View(new ManageLoginsViewModel
        //    {
        //        CurrentLogins = userLogins,
        //        OtherLogins = otherLogins
        //    });
        //}
        //public ActionResult SendMessage()
        //{
        //    ViewBag.Result = "";
        //    var db = new Db.DB_A404D0_mipastaEntities();
        //    var emails = new List<string>(){"mayara.agomez@gmail.com", "juanantonio.butron@hotmail.com"};
        //    List<long> users = db.AppUser.Where(x => emails.Contains(x.Email)).Select(x=> x.Id).ToList();
        //    var model = new MessageModel(){UsersList = Desarrollo.Helpers.Functions.UserList(), IdsList = users};
        //    return View(model);
        //}

     
        // POST: /Manage/LinkLogin
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult LinkLogin(string provider)
        //{
        //    // Solicitar la redirección al proveedor de inicio de sesión externo para vincular un inicio de sesión para el usuario actual
        //    return new AccountController.ChallengeResult(provider, Url.Action("LinkLoginCallback", "Manage"), User.Identity.GetUserId());
        //}

        //
        // GET: /Manage/LinkLoginCallback
//        public async Task<ActionResult> LinkLoginCallback()
//        {
//            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
//            if (loginInfo == null)
//            {
//                return RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
//            }
//            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
//            return result.Succeeded ? RedirectToAction("ManageLogins") : RedirectToAction("ManageLogins", new { Message = ManageMessageId.Error });
//        }

//        protected override void Dispose(bool disposing)
//        {
//            if (disposing && _userManager != null)
//            {
//                _userManager.Dispose();
//                _userManager = null;
//            }

//            base.Dispose(disposing);
//        }

//        public ActionResult MessageControl()
//        {
//            var db = new Db.DB_A404D0_mipastaEntities();
//            var chatModel = new ChatViewModel()
//                {AllMessages = new List<MessageChatModel>(), CurrentMessages = new List<MessageChatModel>()};
//            // se muestran obtiene todos los mensajes agrupados por usuario y ordenados por fecha y nuevo

//            var idLaModerna = db.AppUser.FirstOrDefault(x => x.Email == "moderna.app@lamoderna.com.mx").Id;
//            var comments = db.Database.SqlQuery<MessageChatModel>(@"select TotalMessages, [User], IdUser, Date,New
//                from (select max(date)dd, count(*) TotalMessages,  u.FirstName + ' ' + u.LastName 'User', AppUserId IdUser,CONVERT(VARCHAR(30), max(date),6) 'Date',CAST(MAX(CAST(IsNew as INT)) AS BIT)New
//                from ContactComment cc   inner join AppUser u on cc.AppUserId = u.Id where cc.AppUserId <> "+ idLaModerna + "  group by AppUserId, u.FirstName, u.LastName) res order by new desc  ,dd asc ").ToList();

//            // ingresa primero los nuevos
//            foreach (var m in comments.Where(x=> x.New))
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == 0).ToList();

//                m.Message = ms[0].Message.Length>50 ? ms[0].Message.Substring(0, 50) : ms[0].Message;
//                chatModel.AllMessages.Add(m);
//            }
//            // ingresa primero los que no se han respondido
//            foreach (var m in comments.Where(x => !x.New))
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == idLaModerna).ToList();

//                if (response.Any())
//                    continue;
//                m.Message = ms[0].Message.Length > 50 ? ms[0].Message.Substring(0, 50) : ms[0].Message;
//                chatModel.AllMessages.Add(m);
//            }
//            // ingresa al final  los que ya tienen respuesta
//            foreach (var m in comments.Where(x => !x.New))
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == idLaModerna).ToList();

//                if (!response.Any() || response.Count == 0)
//                    continue; 
//                m.Answered = true;
//                m.Message = ms[0].Message.Length > 50 ? ms[0].Message.Substring(0, 50) : ms[0].Message;
//                chatModel.AllMessages.Add(m);
//            }

//            return View(chatModel);
//        }
//        public ActionResult ViewMessage(ChatViewModel model)
//        {
//            var db = new Db.DB_A404D0_mipastaEntities();
//            var i = 0;
//            var allMessages= new List<MessageChatModel>();

//            var idLaModerna = db.AppUser.FirstOrDefault(x => x.Email == "moderna.app@lamoderna.com.mx").Id;
//            // almacena mensaje nuevo

//            if (model.Message != null)
//            {
//                var idUser = model.IdUser;
//                var lastMessage= db.ContactComment.Where(x => x.AppUserId == idUser).ToList();
//                db.ContactComment.Add(new ContactComment()
//                {
//                    AppUserId = idLaModerna, CommenParentId = lastMessage.LastOrDefault().Id, Date = DateTime.Today, IsNew = true,
//                    Message = model.Message
//                    ,ToAppUserId = model.IdUser
//                });
//            }

//            // actualiza el estatus del mensaje a visto (new=false)
            
//            db.ContactComment.Where(x => x.AppUserId == model.IdUser)
//                .ToList()
//                .ForEach(a=> a.IsNew=false);

//            db.SaveChanges();

//            var chatModel = new ChatViewModel()
//                {AllMessages = new List<MessageChatModel>(), CurrentMessages = new List<MessageChatModel>()};

//            //agrega al principio el mensaje con el que se esta trabajando

//            var curMSG = db.Database.SqlQuery<MessageChatModel>(@"select TotalMessages, [User], IdUser, Date,New
//                from (select max(date)dd, count(*) TotalMessages,  u.FirstName + ' ' + u.LastName 'User', AppUserId IdUser,CONVERT(VARCHAR(30), max(date),6) 'Date',CAST(MAX(CAST(IsNew as INT)) AS BIT)New
//                from ContactComment cc   inner join AppUser u on cc.AppUserId = u.Id  where cc.AppUserId="+model.IdUser +"   group by AppUserId, u.FirstName, u.LastName) res order by new desc  ,dd asc ").ToList();


//            foreach (var m in curMSG)
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == idLaModerna).ToList();

//                if (response.Any())
//                    m.Answered = true;
//                if (ms[0].Message.Length > 50)
//                    m.Message = ms[0].Message.Substring(0, 20);
//                else
//                    m.Message = ms[0].Message;
//                // como se trata del usuario actual obtiene los mensajes a mostrar

//                if (m.IdUser == model.IdUser)
//                {
//                    m.Current = true;
//                    chatModel.IdUser = m.IdUser;
//                    chatModel.CurrentMessages = db.Database.SqlQuery<MessageChatModel>(
//                            @"select cc.Message,CONVERT(VARCHAR(30), date,6) 'Date',case when AppUserId = " + model.IdUser + " then 0 else 1 end Send, u.FirstName + ' ' + u.LastName 'User'"
//                           +" from ContactComment cc   left join AppUser u on cc.AppUserId = u.Id where cc.AppUserId=" + model.IdUser +
//                            " or cc.CommenParentId in (select id from ContactComment where AppUserId=" + model.IdUser + ") order by cc.id asc ")
//                        .ToList();

//                }

//                chatModel.AllMessages.Add(m);
//            }

//            // se muestran obtiene todos los mensajes agrupados por usuario y ordenados por fecha y nuevo a excepcion del que se esta tabajando

//            var comments = db.Database.SqlQuery<MessageChatModel>(@"select TotalMessages, [User], IdUser, Date,New
//                from (select max(date)dd, count(*) TotalMessages,  u.FirstName + ' ' + u.LastName 'User', AppUserId IdUser,CONVERT(VARCHAR(30), max(date),6) 'Date',CAST(MAX(CAST(IsNew as INT)) AS BIT)New
//                from ContactComment cc   inner join AppUser u on cc.AppUserId = u.Id  where cc.AppUserId<> "+model.IdUser+ " and  cc.AppUserId <> " + idLaModerna + "   group by AppUserId, u.FirstName, u.LastName) res order by new desc  ,dd asc ").ToList();


//            // ingresa primero los nuevos
//            foreach (var m in comments.Where(x => x.New))
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == idLaModerna).ToList();

//                m.Message = ms[0].Message.Length > 50 ? ms[0].Message.Substring(0, 50) : ms[0].Message;
             
//                chatModel.AllMessages.Add(m);
//            }
//            // ingresa primero los que no se han respondido
//            foreach (var m in comments.Where(x => !x.New))
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == idLaModerna).ToList();

//                if (response.Any())
//                    continue;
//                m.Message = ms[0].Message.Length > 50 ? ms[0].Message.Substring(0, 50) : ms[0].Message;
            
//                chatModel.AllMessages.Add(m);
//            }
//            // ingresa al final  los que ya tienen respuesta
//            foreach (var m in comments.Where(x => !x.New))
//            {
//                var ms = db.ContactComment.Where(x => x.AppUserId == m.IdUser).OrderByDescending(x => x.Date).ToList();
//                var id = ms[0].Id;
//                var response =
//                    db.ContactComment.Where(x => x.CommenParentId == id && x.AppUserId == idLaModerna).ToList();

//                if (!response.Any() || response.Count == 0)
//                    continue;
//                    m.Answered = true;
//                m.Message = ms[0].Message.Length > 50 ? ms[0].Message.Substring(0, 50) : ms[0].Message;
              
//                chatModel.AllMessages.Add(m);
//            }

           
//            return View("MessageControl",chatModel);
//        }

        #region Aplicaciones auxiliares
        // Se usan para protección XSRF al agregar inicios de sesión externos
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        //private bool HasPassword()
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        return user.PasswordHash != null;
        //    }
        //    return false;
        //}

        //private bool HasPhoneNumber()
        //{
        //    var user = UserManager.FindById(User.Identity.GetUserId());
        //    if (user != null)
        //    {
        //        return user.PhoneNumber != null;
        //    }
        //    return false;
        //}

        public enum ManageMessageId
        {
            AddPhoneSuccess,
            ChangePasswordSuccess,
            SetTwoFactorSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            RemovePhoneSuccess,
            Error
        }

        #endregion
    }
}