﻿using System.Web;
using System.Web.Optimization;

namespace Desarrollo
{
    
        /// <summary>The css rewrite url transform wrapper.</summary>
        public class CssRewriteUrlTransformWrapper : IItemTransform
        {
            /// <summary>The process.</summary>
            /// <param name="includedVirtualPath">The included virtual path.</param>
            /// <param name="input">The input.</param>
            /// <returns>The <see cref="string"/>.</returns>
            public string Process(string includedVirtualPath, string input)
            {
                return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
            }
        }
   
}