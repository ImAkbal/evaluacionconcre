﻿using System.Web;
using System.Web.Optimization;

namespace Desarrollo
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/vendors/jquery/dist/jquery.js"));
                        //,//"~/Scripts/custom.js","~/Scripts/common.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                        "~/vendors/fastclick/lib/fastclick.js",
                        "~/vendors/nprogress/nprogress.js",
                        "~/Scripts/custom.min.js"));
            bundles.Add(new ScriptBundle("~/Content/DataTable").Include(
                "~/Scripts/dataTables.js"));
            bundles.Add(new ScriptBundle("~/bundles/Select2").Include(
                "~/vendors/select2/dist/js/select2.min.js"));

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información. De este modo, estará
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/vendors/font-awesome/css/font-awesome.css",
                      "~/vendors/nprogress/nprogress.css",
                      "~/vendors/animate.css/animate.css",
                      "~/Content/custom.css"));
            bundles.Add(new StyleBundle("~/bundles/Select2css").Include(
                "~/vendors/select2/dist/css/select2.min.css"));

            bundles.Add(
                new StyleBundle("~/Content/DataTableStyle").Include("~/Content/dataTables.css", new CssRewriteUrlTransformWrapper()));

        }
    }
}
