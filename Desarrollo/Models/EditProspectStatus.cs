﻿using Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Desarrollo.Models
{
    public class EditProspectStatusView : Prospect
    {
        public int ProspectId { get; set; }
        public SelectList Statuses { get; set; }

    }
}