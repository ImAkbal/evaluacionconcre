﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Db;

namespace Desarrollo.Models
{
    public class ProspectViewModel : Prospect
    {
        public int ProspectId { get; set; }
        [DisplayName(@"Nombre de Prospecto")]
        public string Name { get; set; }
        public int StatusId { get; set; }
    }
}