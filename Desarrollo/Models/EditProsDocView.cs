﻿using Db;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Desarrollo.Models
{
    public class EditProsDocView : ProspectDocument
    {
        public long ProspectId { get; set; }
        [DisplayName("Upload File")]
        public string FilePath { get; set; }

        public HttpPostedFileBase File { get; set; }

        public List<string> files { get; set; }
    }
}