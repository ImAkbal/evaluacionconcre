﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Desarrollo.Startup))]
namespace Desarrollo 
{ 
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
