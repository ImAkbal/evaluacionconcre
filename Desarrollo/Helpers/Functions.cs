﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml;
using System.IO;


namespace Desarrollo.Helpers
{
    public class Functions
    {
        //public static SelectList Category(int? selectValue = null)
        //{
        //    return new SelectList(new Db.DB_A404D0_mipastaEntities().Category, "Id", "Description", selectValue);
        //}

        //public static SelectList Level(int? selectValue = null)
        //{
        //    return new SelectList(new Db.DB_A404D0_mipastaEntities().Level, "Level", "Level", selectValue);
        //}
        //public static SelectList Unit(int? selectValue = null)
        //{
        //    return new SelectList(new Db.DB_A404D0_mipastaEntities().Unit.Where(x => x.Status == 1), "Id", "Description", selectValue);
        //}

        //public static SelectList Product(int? selectValue = null)
        //{
        //    return new SelectList(new Db.DB_A404D0_mipastaEntities().Products.Select(x => new { Id = x.Id, Product = x.Category.Description + "-" + x.Description + "-" + x.Grammage + "-" + x.Ean }), "Id", "Product", selectValue);   
        //}

        public static byte[] DownloadFileFromFTP(string source, string path)
        {
            var serverPath = "ftp://waws-prod-dm1-139.ftp.azurewebsites.windows.net/site/wwwroot/" + source;

     
            var request = (FtpWebRequest)WebRequest.Create(serverPath);
            request.Method = WebRequestMethods.Ftp.DownloadFile;
            request.Credentials = new NetworkCredential("mipasta\\$mipasta", "x6QTTijNebYPYAymfHC6x9GcaEdYkDoH9nxyaNLtrk7mhmpi5JHtccRENDeY");
            request.UseBinary = true;
      
            using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
            {
                using (Stream responseStream = response.GetResponseStream())
                {
                    using (FileStream fs = new FileStream(path, FileMode.Create))
                    {
                        byte[] buffer = new byte[102400];
                        int read = 0;
                        do
                        {
                            read = responseStream.Read(buffer, 0, buffer.Length);
                            fs.Write(buffer, 0, read);
                            fs.Flush();
                        } while (!(read == 0));

                        fs.Flush();
                        fs.Close();
                    }
                }
            }
            var result=System.IO.File.ReadAllBytes(path);
            File.Delete(path);
            return result;
        }


        public static void UpLoadImage(string image, string target)
        {
            var path = "ftp://waws-prod-dm1-139.ftp.azurewebsites.windows.net/site/wwwroot/" + target;
            FtpWebRequest req = (FtpWebRequest)WebRequest.Create(path);
            req.UseBinary = true;
            req.Method = WebRequestMethods.Ftp.UploadFile;
            req.Credentials = new NetworkCredential("mipasta\\$mipasta", "x6QTTijNebYPYAymfHC6x9GcaEdYkDoH9nxyaNLtrk7mhmpi5JHtccRENDeY");
            byte[] fileData = File.ReadAllBytes(image);

            req.ContentLength = fileData.Length;
            Stream reqStream = req.GetRequestStream();
            reqStream.Write(fileData, 0, fileData.Length);
            reqStream.Close();
            File.Delete(image);
        }

        public static void CreateDirectory(string target)
        {
             var path = "ftp://waws-prod-dm1-139.ftp.azurewebsites.windows.net/site/wwwroot/" + target;
            FtpWebRequest req = (FtpWebRequest)WebRequest.Create(path);
            req.UseBinary = true;
            req.Method = WebRequestMethods.Ftp.MakeDirectory;
            req.Credentials = new NetworkCredential("mipasta\\$mipasta", "x6QTTijNebYPYAymfHC6x9GcaEdYkDoH9nxyaNLtrk7mhmpi5JHtccRENDeY");
            FtpWebResponse CreateForderResponse = (FtpWebResponse)req.GetResponse();
        
        }

        public static void SaveExcel(string pathD, string pathO, List<List<string>> data)
        {
            System.IO.File.Copy(pathO, pathD, true);

            // Open the copied template workbook. 
            using (SpreadsheetDocument myWorkbook = SpreadsheetDocument.Open(pathD, true))
            {
                // Access the main Workbook part, which contains all references.
                WorkbookPart workbookPart = myWorkbook.WorkbookPart;

                // Get the first worksheet. 
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.ElementAt(0);

                // The SheetData object will contain all the data.
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();

                // Begining Row pointer                       
                int index = 2;

                // For each item in the database, add a Row to SheetData.
                foreach (var item in data)
                {
                    // Cell related variable
                    //  string Nom = item.Nom;

                    // New Row
                    Row row = new Row();
                    row.RowIndex = (UInt32)index;
                    char c = 'A';
                    foreach (var field in item)
                    {
                        // New Cell
                        Cell cell = new Cell();
                        cell.DataType = CellValues.InlineString;
                        // Column A1, 2, 3 ... and so on
                        cell.CellReference = c.ToString() + index;

                        // Create Text object
                        Text t = new Text();
                        t.Text = field;

                        // Append Text to InlineString object
                        InlineString inlineString = new InlineString();
                        inlineString.AppendChild(t);

                        // Append InlineString to Cell
                        cell.AppendChild(inlineString);

                        // Append Cell to Row
                        row.AppendChild(cell);
                        c++;
                    }

                    // Append Row to SheetData
                    sheetData.AppendChild(row);

                    // increase row pointer
                    index++;

                }

                // save
                worksheetPart.Worksheet.Save();

            }

        }
        public static SelectList UserList(int? selectValue = 10)
        {
            return new SelectList(new Db.SOSTallerDB().Usuario, "Id", "Email", selectValue);
        }
    }
}